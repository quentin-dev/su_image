FROM ubuntu:disco

RUN apt update
RUN apt install git cmake zip g++ libsfml-dev doxygen -y

RUN git clone https://github.com/ocornut/imgui.git
RUN git clone https://github.com/eliasdaler/imgui-sfml.git

RUN mkdir imgui-sfml/build
WORKDIR imgui-sfml/build

RUN cmake .. -DIMGUI_DIR=/imgui/
RUN cmake --build . --target install
